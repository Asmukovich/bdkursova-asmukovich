﻿using AsmukovichKurs.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace AsmukovichKurs.DB
{
    public static class UserMethods
    {
        public static async Task RegisterUserAsync(DbContext context, string username, string email, string password, string role)
        {
            await context.Database.ExecuteSqlRawAsync(
                "EXEC RegisterUser @p0, @p1, @p2, @p3",
                username, email, password, role
            );
        }
    }

    public static class DeliveryAssignmentMethods
    {
        public static async Task UpdateDeliveryAssignmentAsync(DbContext context, DeliveryAssignment deliveryAssignment)
        {
            context.Update(deliveryAssignment);
            await context.SaveChangesAsync();

            if (deliveryAssignment.Status == "Delivered")
            {
                var orderIdParam = new SqlParameter("@OrderId", deliveryAssignment.OrderId);
                var statusParam = new SqlParameter("@Status", "Completed");

                await context.Database.ExecuteSqlRawAsync("EXEC UpdateOrderStatus @OrderId, @Status",
                                       orderIdParam, statusParam);
            }

            await DeliveryDriverMethods.UpdateDriverAvailabilityAsync(context, deliveryAssignment.DriverId, true);
        }
    }

    public static class DeliveryDriverMethods
    {
        public static async Task AddDeliveryDriverAsync(DbContext context, DeliveryDriver deliveryDriver)
        {
            await context.Database.ExecuteSqlRawAsync(
                "EXEC AddDeliveryDriver @UserId, @VehicleType, @LicencePlate, @Available",
                     new SqlParameter("@UserId", deliveryDriver.UserId),
                     new SqlParameter("@VehicleType", deliveryDriver.VehicleType),
                     new SqlParameter("@LicencePlate", deliveryDriver.LicencePlate),
                     new SqlParameter("@Available", deliveryDriver.Available));
        }

        public static async Task UpdateDriverAvailabilityAsync(DbContext context, int driverId, bool available)
        {
            await context.Database.ExecuteSqlRawAsync(
                "EXEC UpdateDriverAvailability @p0, @p1",
                    driverId, available);
        }
    }

    public static class OrderItemsMethods
    {
        public static async Task AddOrderItemAsync(DbContext context, int orderId, int productId, int quantity)
        {
            var orderIdParam = new SqlParameter("@OrderId", orderId);
            var productIdParam = new SqlParameter("@ProductId", productId);
            var quantityParam = new SqlParameter("@Quantity", quantity);

            await context.Database.ExecuteSqlRawAsync("EXEC AddOrderItem @OrderId, @ProductId, @Quantity",
                               orderIdParam, productIdParam, quantityParam);
        }
    }

    public static class OrderMethods
    {
        public static async Task<int> PlaceOrderAsync(DbContext context, int userId, int addressId, decimal totalAmount)
        {
            var orderId = new SqlParameter
            {
                ParameterName = "@OrderId",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };

            await context.Database.ExecuteSqlRawAsync(
                "EXEC PlaceOrder @UserId, @AddressId, @TotalAmount, @OrderId OUT",
                    new SqlParameter("@UserId", userId),
                    new SqlParameter("@AddressId", addressId),
                    new SqlParameter("@TotalAmount", totalAmount),
                    orderId);

            return (int)orderId.Value;
        }

        public static async Task AssignDriverAsync(DbContext context, int orderId, int driverId)
        {
            await context.Database.ExecuteSqlRawAsync(
                "EXEC AssignDriver @p0, @p1",
                    orderId, driverId);
        }
    }
    public static class PaymentMethods
    { 
        public static async Task ProcessPaymentAsync(DbContext context, int orderId, decimal amount, string paymentMethod, string status)
        {
            var orderIdParam = new SqlParameter("@OrderId", orderId);
            var amountParam = new SqlParameter("@Amount", amount);
            var paymentMethodParam = new SqlParameter("@PaymentMethod", paymentMethod);
            var statusParam = new SqlParameter("@Status", status);

            await context.Database.ExecuteSqlRawAsync("EXEC ProcessPayment @OrderId, @Amount, @PaymentMethod, @Status",
                               orderIdParam, amountParam, paymentMethodParam, statusParam);
        }
    }

    public class AppDBContext : DbContext
	{
		public DbSet<Address>? Address { get; set; }
		public DbSet<DeliveryDriver>? DeliveryDrivers { get; set; }
		public DbSet<Order>? Orders { get; set; }
		public DbSet<OrderItem>? OrderItems { get; set; }
		public DbSet<Payment>? Payments { get; set; }
		public DbSet<Product>? Products { get; set; }
		public DbSet<User>? Users { get; set; }
        public DbSet<DeliveryAssignment>? DeliveryAssignment { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseSqlServer("Data Source=DESKTOP-MD0OROT;Database=DeliveryDB;Trusted_Connection=True;TrustServerCertificate=True");
			}
		}
        public async Task RegisterUserAsync(string username, string email, string password, string role) => await UserMethods.RegisterUserAsync(this, username, email, password, role);
        public async Task AddDeliveryDriverAsync(DeliveryDriver deliveryDriver) => await DeliveryDriverMethods.AddDeliveryDriverAsync(this, deliveryDriver);
        public async Task UpdateDeliveryAssignmentAsync(DeliveryAssignment deliveryAssignment) => await DeliveryAssignmentMethods.UpdateDeliveryAssignmentAsync(this, deliveryAssignment);
        public async Task AddOrderItemAsync(int orderId, int productId, int quantity) => await OrderItemsMethods.AddOrderItemAsync(this, orderId, productId, quantity);
        public async Task<int> PlaceOrderAsync(int userId, int addressId, decimal totalAmount) => await OrderMethods.PlaceOrderAsync(this, userId, addressId, totalAmount);
        public async Task ProcessPaymentAsync(int orderId, decimal amount, string paymentMethod, string status) => await PaymentMethods.ProcessPaymentAsync(this, orderId, amount, paymentMethod, status);
        public async Task AssignDriverAsync(int orderId, int driverId) => await OrderMethods.AssignDriverAsync(this, orderId, driverId);
        public async Task UpdateDriverAvailabilityAsync(int driverId, bool available) => await DeliveryDriverMethods.UpdateDriverAvailabilityAsync(this, driverId, available);
    }
}
