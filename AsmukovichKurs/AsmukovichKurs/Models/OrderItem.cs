﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
    [Table("order_items")]
    public class OrderItem
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("order_id")]
        public int OrderId { get; set; }
        public virtual Order? Order { get; set; }

        [Required]
        [Column("product_id")]
        public int ProductId { get; set; }
        public virtual Product? Product { get; set; }

        [Required]
        [Column("quantity")]
        public int Quantity { get; set; }
    }
}
