﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
    [Table("delivery_drivers")]
    public class DeliveryDriver
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("user_id")]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User? User { get; set; }

        [Required]
        [Column("vehicle_type")]
        public string? VehicleType { get; set; }

        [Required]
        [Column("licence_plate")]
        public string? LicencePlate { get; set; }

        [Column("available")]
        public bool Available { get; set; } = true;
    }
}
