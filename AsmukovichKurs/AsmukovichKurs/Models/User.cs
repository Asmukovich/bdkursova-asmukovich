﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
    [Table("users")]
    public class User
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("username")]
        [Required(ErrorMessage = "Username is required.")]
        public string? Username { get; set; }

        [Column("email")]
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string? Email { get; set; }

        [Column("password")]
        [Required(ErrorMessage = "Password is required.")]
        public string? Password { get; set; }

        [Column("role")]
        [Required(ErrorMessage = "Role is required.")]
        public string? Role { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
