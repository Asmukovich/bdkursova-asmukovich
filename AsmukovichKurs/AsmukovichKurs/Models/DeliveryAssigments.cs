﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
    [Table("delivery_assignments")]
    public class DeliveryAssignment
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("order_id")]
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public Order? Order { get; set; }

        [Column("driver_id")]
        [ForeignKey("DeliveryDriver")]
        public int DriverId { get; set; }
        public DeliveryDriver? DeliveryDriver { get; set; }
        public string? DriverLicenseNumber => DeliveryDriver?.LicencePlate;
        public string? DriverUsername => DeliveryDriver?.User?.Username;

        [Column("address_id")]
        [ForeignKey("Address")]
        public int AddressId { get; set; }
        public Address? Address { get; set; }

        [Column("status")]
        public string? Status { get; set; }
    }
}
