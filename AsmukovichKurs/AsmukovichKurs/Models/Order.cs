﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
    [Table("orders")]
    public class Order
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User? User { get; set; }

        [Column("address_id")]
        public int AddressId { get; set; }

        [ForeignKey("AddressId")]
        public Address? Address { get; set; }

        [Column("status")]
        [StringLength(100)]
        public string? Status { get; set; }

        [Column("total_amount")]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; } = 0;

        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
