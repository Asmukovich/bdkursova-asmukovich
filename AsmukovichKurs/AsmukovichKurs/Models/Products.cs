﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{
	[Table("products")]
	public class Product
	{
		[Column("id")]
		public int Id { get; set; }

        [Column("name")]
        [Required(ErrorMessage = "The name field is required.")]
        public string? Name { get; set; }

        [Column("description")]
		public string? Description { get; set; }

		[Column("price")]
		public decimal Price { get; set; }
	}
}
