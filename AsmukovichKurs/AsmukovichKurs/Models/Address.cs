﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AsmukovichKurs.Models
{

	[Table("adresses")]
	public class Address
	{
		[Column("id")]
		public int Id { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User? User { get; set; }

        [Column("address_line1")]
		public required string AddressLine1 { get; set; }

		[Column("address_line2")]
		public string? AddressLine2 { get; set; }

		[Column("city")]
		public string? City { get; set; }

		[Column("state")]
		public string? State { get; set; }

		[Column("zip_code")]
		public string? ZipCode { get; set; }
	}

}
