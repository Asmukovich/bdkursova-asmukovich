﻿using AsmukovichKurs.DB;
using AsmukovichKurs.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

public class OrderItemsController : Controller
{
    private readonly AppDBContext _context;

    public OrderItemsController(AppDBContext context)
    {
        _context = context;
    }

    // GET: OrderItems
    public async Task<IActionResult> Index()
    {
        var appDBContext = _context.OrderItems.Include(o => o.Order).Include(o => o.Product);
        return View(await appDBContext.ToListAsync());
    }

    // GET: OrderItems/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var orderItem = await _context.OrderItems
            .Include(o => o.Order)
            .Include(o => o.Product)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (orderItem == null)
        {
            return NotFound();
        }

        return View(orderItem);
    }

    // GET: OrderItems/Create
    public IActionResult Create()
    {
        ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id"); // Використовуйте властивість "Id" для відображення
        ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name");
        return View();
    }

    // POST: OrderItems/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("OrderId,ProductId,Quantity")] OrderItem orderItem)
    {
        if (ModelState.IsValid)
        {
            try
            {
                await _context.AddOrderItemAsync(orderItem.OrderId, orderItem.ProductId, orderItem.Quantity);
                return RedirectToAction(nameof(Index));
            }
            catch (SqlException ex)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
        }
        ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", orderItem.OrderId);
        ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", orderItem.ProductId);
        return View(orderItem);
    }

    // GET: OrderItems/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var orderItem = await _context.OrderItems.FindAsync(id);
        if (orderItem == null)
        {
            return NotFound();
        }
        ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "OrderSummary", orderItem.OrderId);
        ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", orderItem.ProductId);
        return View(orderItem);
    }

    // POST: OrderItems/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,OrderId,ProductId,Quantity")] OrderItem orderItem)
    {
        if (id != orderItem.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(orderItem);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderItemExists(orderItem.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }
        ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "OrderSummary", orderItem.OrderId);
        ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", orderItem.ProductId);
        return View(orderItem);
    }

    // GET: OrderItems/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var orderItem = await _context.OrderItems
            .Include(o => o.Order)
            .Include(o => o.Product)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (orderItem == null)
        {
            return NotFound();
        }

        return View(orderItem);
    }

    // POST: OrderItems/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var orderItem = await _context.OrderItems.FindAsync(id);
        if (orderItem != null)
        {
            _context.OrderItems.Remove(orderItem);
            await _context.SaveChangesAsync();
        }
        return RedirectToAction(nameof(Index));
    }

    private bool OrderItemExists(int id)
    {
        return _context.OrderItems.Any(e => e.Id == id);
    }
}
