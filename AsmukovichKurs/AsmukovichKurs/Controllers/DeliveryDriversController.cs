﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AsmukovichKurs.DB;
using AsmukovichKurs.Models;
using Microsoft.Data.SqlClient;

namespace AsmukovichKurs.Controllers
{
    public class DeliveryDriversController : Controller
    {
        private readonly AppDBContext _context;

        public DeliveryDriversController(AppDBContext context)
        {
            _context = context;
        }

        // GET: DeliveryDrivers
        public async Task<IActionResult> Index()
        {
            var appDBContext = _context.DeliveryDrivers;
            return View(await appDBContext.ToListAsync());
        }

        public async Task<IActionResult> AssignDriver(int orderId, int driverId)
        {
            if (ModelState.IsValid)
            {
                await _context.AssignDriverAsync(orderId, driverId);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        public async Task<IActionResult> UpdateDriverAvailability(int driverId, bool available)
        {
            if (ModelState.IsValid)
            {
                await _context.UpdateDriverAvailabilityAsync(driverId, available);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: DeliveryDrivers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryDriver = await _context.DeliveryDrivers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryDriver == null)
            {
                return NotFound();
            }

            return View(deliveryDriver);
        }

        // GET: DeliveryDrivers/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username");
            return View();
        }

        // POST: DeliveryDrivers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,VehicleType,LicencePlate,Available")] DeliveryDriver deliveryDriver)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.AddDeliveryDriverAsync(deliveryDriver);
                    return RedirectToAction(nameof(Index));
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 50000) // Custom error number from RAISERROR
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                    }
                }
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username", deliveryDriver.UserId);
            return View(deliveryDriver);
        }


        // GET: DeliveryDrivers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryDriver = await _context.DeliveryDrivers.FindAsync(id);
            if (deliveryDriver == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username", deliveryDriver.UserId);
            return View(deliveryDriver);
        }

        // POST: DeliveryAssignments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,OrderId,DriverId,AddressId,Status")] DeliveryAssignment deliveryAssignment)
        {
            if (id != deliveryAssignment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.UpdateDeliveryAssignmentAsync(deliveryAssignment);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliveryAssignmentExists(deliveryAssignment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", deliveryAssignment.AddressId);
            ViewData["DriverId"] = new SelectList(_context.DeliveryDrivers, "Id", "LicencePlate", deliveryAssignment.DriverId);
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", deliveryAssignment.OrderId);
            ViewData["Status"] = new SelectList(new List<string> { "Pending", "In Transit", "Delivered" }, deliveryAssignment.Status);
            return View(deliveryAssignment);
        }


        // GET: DeliveryDrivers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryDriver = await _context.DeliveryDrivers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryDriver == null)
            {
                return NotFound();
            }

            return View(deliveryDriver);
        }

        // POST: DeliveryDrivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deliveryDriver = await _context.DeliveryDrivers.FindAsync(id);
            if (deliveryDriver != null)
            {
                _context.DeliveryDrivers.Remove(deliveryDriver);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliveryAssignmentExists(int id)
        {
            return _context.DeliveryAssignment.Any(e => e.Id == id);
        }
    }
}
