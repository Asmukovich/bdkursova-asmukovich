﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AsmukovichKurs.DB;
using AsmukovichKurs.Models;

namespace AsmukovichKurs.Controllers
{
    public class OrdersController : Controller
    {
        private readonly AppDBContext _context;

        public OrdersController(AppDBContext context)
        {
            _context = context;
        }

        // GET: Orders
        public async Task<IActionResult> Index()
        {
            var appDBContext = _context.Orders;
            return View(await appDBContext.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public IActionResult Create()
        {
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1"); // Переконайтеся, що в моделі Address є властивість FullAddress
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username"); // Username або інша властивість, яка ідентифікує користувача
            ViewData["Status"] = new SelectList(new List<string> { "Created", "Payed", "Shipping", "Delivered", "Canceled" }); // Додайте можливі статуси
            return View();
        }

        // POST: Orders/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,AddressId,Status")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.CreatedAt = DateTime.Now;  // Встановлення дати створення
                order.TotalAmount = 0; // Встановлення суми на 0
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username", order.UserId);
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", order.AddressId);
            ViewData["Status"] = new SelectList(new List<string> { "Created", "Payed", "Shipping", "Delivered", "Canceled" }, order.Status);
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username", order.UserId);
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", order.AddressId);
            ViewData["Status"] = new SelectList(new List<string> { "Created", "Payed", "Shipping", "Delivered", "Canceled" }, order.Status); // Додайте можливі статуси
            return View(order);
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,AddressId,Status,CreatedAt")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Username", order.UserId);
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", order.AddressId);

            return View(order);
        }


        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            if (order != null)
            {
                try
                {
                    _context.Orders.Remove(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException ex)
                {
                    // Логування ексепшну, наприклад, через ILogger
                    ModelState.AddModelError("", "Unable to delete. It may have related items that prevent it from being deleted.");
                    return View("Delete", order);
                }
            }

            return RedirectToAction(nameof(Index));
        }

    }
}
