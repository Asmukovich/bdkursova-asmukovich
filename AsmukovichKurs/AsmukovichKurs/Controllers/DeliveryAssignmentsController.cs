﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AsmukovichKurs.DB;
using AsmukovichKurs.Models;
using Microsoft.Data.SqlClient;

namespace AsmukovichKurs.Controllers
{
    public class DeliveryAssignmentsController : Controller
    {
        private readonly AppDBContext _context;

        public DeliveryAssignmentsController(AppDBContext context)
        {
            _context = context;
        }

        // GET: DeliveryAssignments
        public async Task<IActionResult> Index()
        {
            var appDBContext = _context.DeliveryAssignment.Include(d => d.Address).Include(d => d.DeliveryDriver).Include(d => d.Order);
            return View(await appDBContext.ToListAsync());
        }

        // GET: DeliveryAssignments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryAssignment = await _context.DeliveryAssignment
                .Include(d => d.Address)
                .Include(d => d.DeliveryDriver)
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryAssignment == null)
            {
                return NotFound();
            }

            return View(deliveryAssignment);
        }

        // GET: DeliveryAssignments/Create
        public IActionResult Create()
        {
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1");
            ViewData["DriverId"] = new SelectList(_context.DeliveryDrivers, "Id", "LicencePlate");
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id");
            ViewData["Status"] = new SelectList(new List<string> { "Pending", "In Transit", "Delivered" });
            return View();
        }

        // POST: DeliveryAssignments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,DriverId,AddressId,Status")] DeliveryAssignment deliveryAssignment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(deliveryAssignment);
                await _context.SaveChangesAsync();

                await _context.AssignDriverAsync(deliveryAssignment.OrderId, deliveryAssignment.DriverId);

                return RedirectToAction(nameof(Index));
            }
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", deliveryAssignment.AddressId);
            ViewData["DriverId"] = new SelectList(_context.DeliveryDrivers, "Id", "LicencePlate", deliveryAssignment.DriverId);
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", deliveryAssignment.OrderId);
            ViewData["Status"] = new SelectList(new List<string> { "Pending", "In Transit", "Delivered" }, deliveryAssignment.Status);
            return View(deliveryAssignment);
        }

        // GET: DeliveryAssignments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryAssignment = await _context.DeliveryAssignment.FindAsync(id);
            if (deliveryAssignment == null)
            {
                return NotFound();
            }
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", deliveryAssignment.AddressId);
            ViewData["DriverId"] = new SelectList(_context.DeliveryDrivers, "Id", "LicencePlate", deliveryAssignment.DriverId);
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", deliveryAssignment.OrderId);
            ViewData["Status"] = new SelectList(new List<string> { "Pending", "In Transit", "Delivered" }, deliveryAssignment.Status);
            return View(deliveryAssignment);
        }


        // POST: DeliveryAssignments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,OrderId,DriverId,AddressId,Status")] DeliveryAssignment deliveryAssignment)
        {
            if (id != deliveryAssignment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.UpdateDeliveryAssignmentAsync(deliveryAssignment);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliveryAssignmentExists(deliveryAssignment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AddressId"] = new SelectList(_context.Address, "Id", "AddressLine1", deliveryAssignment.AddressId);
            ViewData["DriverId"] = new SelectList(_context.DeliveryDrivers, "Id", "LicencePlate", deliveryAssignment.DriverId);
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", deliveryAssignment.OrderId);
            ViewData["Status"] = new SelectList(new List<string> { "Pending", "In Transit", "Delivered" }, deliveryAssignment.Status);
            return View(deliveryAssignment);
        }



        // GET: DeliveryAssignments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryAssignment = await _context.DeliveryAssignment
                .Include(d => d.Address)
                .Include(d => d.DeliveryDriver)
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryAssignment == null)
            {
                return NotFound();
            }

            return View(deliveryAssignment);
        }

        // POST: DeliveryAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deliveryAssignment = await _context.DeliveryAssignment.FindAsync(id);
            if (deliveryAssignment != null)
            {
                _context.DeliveryAssignment.Remove(deliveryAssignment);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliveryAssignmentExists(int id)
        {
            return _context.DeliveryAssignment.Any(e => e.Id == id);
        }
    }
}
