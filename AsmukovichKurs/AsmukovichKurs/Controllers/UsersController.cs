﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AsmukovichKurs.DB;
using AsmukovichKurs.Models;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AsmukovichKurs.Controllers
{
    public class UsersController : Controller
    {
        private readonly AppDBContext _context;

        public UsersController(AppDBContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            return View(await _context.Users.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            ViewData["Role"] = new SelectList(new List<string> { "Admin", "User", "Driver" });
            return View();
        }


        // POST: Users/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Email,Password,Role,CreatedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.RegisterUserAsync(user.Username, user.Email, user.Password, user.Role);
                    return RedirectToAction(nameof(Index));
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2627 || ex.Number == 2601) // SQL error numbers for unique constraint violation
                    {
                        ModelState.AddModelError("", "Username or Email already exists.");
                    }
                    else if (ex.Number == 50000) // Custom error number from RAISERROR
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                    else
                    {
                        throw; // Unhandled errors
                    }
                }
            }
            ViewData["Role"] = new SelectList(new List<string> { "Admin", "User", "Driver" }, user.Role);
            return View(user);
        }


        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["Role"] = new SelectList(new List<string> { "Admin", "User", "Driver" }, user.Role);
            return View(user);
        }


        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        // POST: Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Email,Password,Role,CreatedAt")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (DbUpdateException ex)
                {
                    if (ex.InnerException is SqlException sqlEx && (sqlEx.Number == 2627 || sqlEx.Number == 2601)) // Unique constraint violation
                    {
                        ModelState.AddModelError("", "Username or Email already exists.");
                    }
                    else
                    {
                        throw; // Unhandled errors
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Role"] = new SelectList(new List<string> { "Admin", "User", "Driver" }, user.Role);
            return View(user);
        }


        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var result = await _context.Database.ExecuteSqlRawAsync("EXEC DeleteUser @UserId", new SqlParameter("@UserId", id));
                if (result == 0)
                {
                    TempData["ErrorMessage"] = "Unable to delete user due to associated records.";
                    var user = await _context.Users.FindAsync(id);
                    return View("Delete", user);
                }
            }
            catch (SqlException ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                var user = await _context.Users.FindAsync(id);
                return View("Delete", user);
            }

            TempData["SuccessMessage"] = "User deleted successfully.";
            return RedirectToAction(nameof(Index));
        }

    }
}
