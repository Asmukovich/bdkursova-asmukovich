using AsmukovichKurs.DB;
using AsmukovichKurs.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace AsmukovichKurs.Controllers
{
	public class HomeController : Controller
	{
		private readonly AppDBContext _context;

		public HomeController(AppDBContext context)
		{
			_context = context;
		}

		public async Task<IActionResult> Index()
		{
			var products = _context.Products;
			return View(await products.ToListAsync()); 
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
